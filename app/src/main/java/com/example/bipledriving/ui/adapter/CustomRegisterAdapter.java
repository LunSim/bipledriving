package com.example.bipledriving.ui.adapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.bipledriving.R;
import com.example.bipledriving.model.home.HomeModelRespond;
import com.example.bipledriving.model.login.Singleton;
import com.example.bipledriving.ui.register.RegisterServiceActivity;
import java.util.ArrayList;
import java.util.Calendar;

public class CustomRegisterAdapter extends RecyclerView.Adapter<CustomRegisterAdapter.ViewHolder>{
    private ArrayList<HomeModelRespond> mHomeModelResponds;
    private Context context;
    private String USE_TYPE = "";

    public CustomRegisterAdapter(ArrayList<HomeModelRespond> mHomeModelResponds, Context context) {
        this.mHomeModelResponds = mHomeModelResponds;
        this.context = context;
    }

    public void setdate(ArrayList<HomeModelRespond> homeModelResponds){
       this.mHomeModelResponds = homeModelResponds;
    }

    //create object ViewHolder and convert layout
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.adapter_listview, parent, false);
        return new ViewHolder(v);
    }

    //use for bind data into layout
    //first we get dataSource
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        HomeModelRespond homeModelRespond = mHomeModelResponds.get(position);
//        holder.tv_date_api.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_transition_animation));
        holder.layout.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_transition_animation));

        holder.tv_date_api.setText(getKrDateformat(homeModelRespond.getYYYYMMDD()));
        switch ((homeModelRespond.getUSE_TYPE())){
            case "1":
                USE_TYPE = homeModelRespond.setUSE_TYPE("일반업무용");
                break;
            case "2":
                USE_TYPE = homeModelRespond.setUSE_TYPE("출퇴근용");
                break;
            case "3":
                USE_TYPE = homeModelRespond.setUSE_TYPE("비업무용");
                break;
        }
        holder.tv_commute.setText(USE_TYPE);
        holder.tv_res_commute_far.setText(homeModelRespond.getDRV_DSTN_KM());
        holder.itemView.setOnClickListener(view -> {
            //send data vai singleton
            Singleton.getInstance().setHomeModelRespond(homeModelRespond);
            Intent intent = new Intent(context,RegisterServiceActivity.class);
            context.startActivity(intent);
            ((AppCompatActivity)context).finish();
        });
    }

    @Override
    public int getItemCount() {
        return this.mHomeModelResponds.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {
        private TextView tv_date_api,tv_commute,tv_res_commute_far;
        RelativeLayout layout;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_date_api = itemView.findViewById(R.id.tv_date_api);
            tv_commute = itemView.findViewById(R.id.tv_commute);
            tv_res_commute_far = itemView.findViewById(R.id.tv_res_commute_far);
            layout = itemView.findViewById(R.id.main_layout1);
        }
    }

    /**
     * Ex.
     * Input : YYYYMMDD
     * Output: 11월27일(화)
     * @return krDate
     */
    private String getKrDateformat(String date){
        String krDate = "";
        if(!date.equals("")) {
            krDate = date.substring(4,6)+context.getResources().getString(R.string.MAIN_04)+date.substring(6) + context.getResources().getString(R.string.MAIN_05)+"(" +getDayOfWeek(date) + ")";
        }

        return krDate;
    }

    /**
     * 해당일자의 요일을 반환
     * <br/><br/>
     * - 일자포맷 : YYYYMMDD<br>
     *
     * @param aDate (YYYYMMDD)
     * @return 요일 (월,화,수,목,금,토,일)
     */
    public  String getDayOfWeek(String aDate) {
        String strDate = aDate.replace(".", "");
        String strReturn = "";
        int Year = Integer.parseInt(strDate.substring(0, 4));
        int Month = Integer.parseInt(strDate.substring(4, 6));
        int Date = Integer.parseInt(strDate.substring(6, 8));
        Calendar cal = Calendar.getInstance();
        cal.set(Year, Month - 1, Date);
        switch (cal.get(Calendar.DAY_OF_WEEK)) {
            case  Calendar.SUNDAY :
                strReturn = "일";
                break;
            case Calendar.MONDAY :
                strReturn = "월";
                break;
            case Calendar.TUESDAY :
                strReturn = "화";
                break;
            case Calendar.WEDNESDAY :
                strReturn = "수";
                break;
            case Calendar.THURSDAY :
                strReturn = "목";
                break;
            case Calendar.FRIDAY :
                strReturn = "금";
                break;
            case Calendar.SATURDAY :
                strReturn = "토";
                break;
        }
        return strReturn;
    }
}
