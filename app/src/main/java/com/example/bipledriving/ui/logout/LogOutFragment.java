package com.example.bipledriving.ui.logout;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Window;
import android.widget.Button;
import com.example.bipledriving.R;
import com.example.bipledriving.helper.Comtran;
import com.example.bipledriving.model.login.Singleton;
import com.example.bipledriving.tx.logout.TX_CARMN_0001_REQ;
import com.example.bipledriving.ui.login.LoginActivity;
import static android.content.Context.MODE_PRIVATE;

public class LogOutFragment extends Dialog implements Comtran.OnComTranListener {
    private Activity activity;
    private SharedPreferences.Editor editor;
    private Button btn_LogOut,btn_cancle;

    public LogOutFragment(@NonNull Context context, Activity activity) {
        super(context);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.fragment_log_out);

        btn_cancle = findViewById(R.id.btn_cancle);
        btn_LogOut = findViewById(R.id.btn_logout);

        btn_LogOut.setOnClickListener(view -> {
            try{
                getLogOut();
                SharedPreferences sharedPreferences = getContext().getSharedPreferences("bipledriving",MODE_PRIVATE);
                editor = sharedPreferences.edit();
                editor.remove("user");
                editor.remove("pass").apply();
                Singleton.getInstance().setUser(null);
            }catch (Exception e){
                e.getStackTrace();
            }
            Intent intent = new Intent(getContext(), LoginActivity.class);
            getContext().startActivity(intent);
            activity.finish();
        });

        btn_cancle.setOnClickListener(view -> {
            cancel();
        });

    }

    public void getLogOut(){
        Comtran comtran = new Comtran(getContext(),this);
        try {
            TX_CARMN_0001_REQ req0001 = new TX_CARMN_0001_REQ();
            req0001.setUSER_ID(Singleton.getInstance().getUser().getId());
            comtran.request(TX_CARMN_0001_REQ.TXNO, req0001.getSendMessage(), false);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onTranRespond(String tranCode, Object object) {
        Log.e(">>>","LOGOsbookUT::"+object.toString());
    }

    @Override
    public void onTranError(String tranCode) {

    }
}