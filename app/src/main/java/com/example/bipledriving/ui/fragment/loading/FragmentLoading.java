package com.example.bipledriving.ui.fragment.loading;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.bipledriving.R;

public class FragmentLoading extends DialogFragment {
    ImageView imgLoading;
    AnimationDrawable animationDrawable;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_loading,container,false);
        imgLoading = view.findViewById(R.id.loadingF);
        // Get the background, which has been compiled to an AnimationDrawable object.
        animationDrawable = (AnimationDrawable) imgLoading.getBackground();
        // make dialog itself transparent
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // Start the animation (looped playback by default).
        animationDrawable.start();
        return view;
    }
}
