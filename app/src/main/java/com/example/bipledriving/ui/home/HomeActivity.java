package com.example.bipledriving.ui.home;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.bipledriving.R;
import com.example.bipledriving.com.FlexibleMenu;
import com.example.bipledriving.helper.Comtran;
import com.example.bipledriving.model.home.HomeModelRespond;
import com.example.bipledriving.tx.api_home_request.TX_CARMN_1001_REQ;
import com.example.bipledriving.tx.api_home_request.TX_CARMN_1001_RES;
import com.example.bipledriving.tx.api_home_request.TX_CARMN_1001_RES_REC;
import com.example.bipledriving.ui.adapter.CustomRegisterAdapter;
import com.example.bipledriving.ui.fragment.FragmentFilter;
import com.example.bipledriving.ui.fragment.loading.FragmentLoading;
import com.example.bipledriving.ui.logout.LogOutFragment;
import com.github.clans.fab.FloatingActionButton;
import java.util.ArrayList;
import khmertrainers.com.mylibrary.toolbar.FlexibleToolBar;

    public class HomeActivity extends AppCompatActivity implements View.OnClickListener, Comtran.OnComTranListener {
    private FlexibleToolBar mToolBar;
    private LogOutFragment logOutFragment;
    private FloatingActionButton btn_add_new,btn_call_driver;
    private Button btn_LogOut;
    private Comtran comtran;
    private RecyclerView recyclerView;
    private ArrayList<HomeModelRespond> homeModelRespondList = new ArrayList<>();
    private CustomRegisterAdapter adapter;
    private HomeModelRespond homeModelRespond;
    private FragmentLoading fragmentLoading;
    private TextView no_data,user_name,biz_name;
    private ImageView img_Filter;
    private FragmentFilter fragmentFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initialization();
        logOutFragment  =  new LogOutFragment(this,this);

        mToolBar.setToolBarButton(FlexibleToolBar.GB_LEFT,getResources().getString(R.string.MAIN_01),20,0);
        mToolBar.setToolBarButton(FlexibleToolBar.GB_RIGHT,R.drawable.ic_menu,0,20);
        mToolBar.setOnToolBarClickListener((groupButton, view) ->  {
            if (groupButton == FlexibleToolBar.GB_RIGHT){
                switch (view.getId()){
                    case 1:
                        showPopUpItem(view);
                        break;
                }
            }
        });

        //set recyclerView
        recyclerView  = findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new CustomRegisterAdapter(homeModelRespondList, this);
        recyclerView.setAdapter(adapter);

        //register event
        btn_add_new.setOnClickListener(this);
        img_Filter.setOnClickListener(this);
        /*Call respond api*/
        requestHomeAPI();

    }

    public void initialization(){
        mToolBar        =  findViewById(R.id.toolbar);
        btn_LogOut      =  findViewById(R.id.btn_logout);
        btn_add_new     =  findViewById(R.id.add_new);
        no_data         =  findViewById(R.id.no_data);
        btn_call_driver =  findViewById(R.id.item_car);
        img_Filter       =  findViewById(R.id.imgFilter);
        user_name       =  findViewById(R.id.user_name);
        biz_name        =  findViewById(R.id.biz_name);
    }


    //generate menu bar
    public void showPopUpItem(View view){
        String[] items = {"로그아웃"};
        FlexibleMenu mMenuPopup = new FlexibleMenu(HomeActivity.this, 0, 360, 150, 10);
        mMenuPopup.setMenuPadding(40, 0, 0, 0);
        mMenuPopup.setMenuShow(FlexibleMenu.SHOW_AS_DROPDOWN,FlexibleMenu.RIGHT_ANCHOR,view,190,0);
        mMenuPopup.setMenuItem(items, 15, R.color.color_263238, R.color.color_f87499, R.color.color_eff1f5, true);
        mMenuPopup.setOnMenuItemClickListener(new FlexibleMenu.onFlexibleMenuListener() {
            @Override
            public void onMenuItemClicked(int index) {
                switch (index){
                    case 0:
                        logOutFragment.show();
                        break;
                }
            }
        });
    }

    //event floating button
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.add_new:
                Toast.makeText(this, "Add new!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.item_car:
                Toast.makeText(this, "View Car", Toast.LENGTH_SHORT).show();
                break;
            case R.id.imgFilter:
                    fragmentFilter = new FragmentFilter();
                    FragmentManager manager = getSupportFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    transaction.replace(R.id.fragmentFilter,fragmentFilter);
                    transaction.commit();
                break;
        }
    }

    //request api
    public void requestHomeAPI(){
        //Loading
        fragmentLoading = new FragmentLoading();
        fragmentLoading.show(getSupportFragmentManager(),"FragmentLoading");
        comtran = new Comtran(this,this);
            try {
                TX_CARMN_1001_REQ req1001 = new TX_CARMN_1001_REQ();
                req1001.setBIZ_NO("2148635102");
                req1001.setUSER_ID("sbook02");
                req1001.setSTR_DT("20190909");
                req1001.setEND_DT("20191009");
                req1001.setUSE_TYPE("");
                req1001.setCAR_NO("");
                req1001.setYYYYMMDD("");
                req1001.setSQNO("");
                req1001.setSRCH_WD("");
                comtran.request(TX_CARMN_1001_REQ.TXNO, req1001.getSendMessage(), false);
                Log.e(">>>>","Home:::"+req1001.getSendMessage());
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    //request success
    @Override
    public void onTranRespond(String tranCode, Object object) {

        new Handler().postDelayed(()-> {
            fragmentLoading.dismiss();
            try {
                TX_CARMN_1001_RES  res1001 = new TX_CARMN_1001_RES(object);
                TX_CARMN_1001_RES_REC resRec = res1001.getDRIVE_REC();
                for (int i =0 ; i < resRec.getLength(); i++){
                    res1001.getDRIVE_REC().getSQNO();
                    res1001.getDRIVE_REC().getBIZ_NO();
                    res1001.getDRIVE_REC().getBIZ_NM();
                    res1001.getDRIVE_REC().getCAR_NO();
                    res1001.getDRIVE_REC().getCAR_TYPE();
                    res1001.getDRIVE_REC().getMM();
                    res1001.getDRIVE_REC().getYYYYMMDD();
                    res1001.getDRIVE_REC().getDVSN_NM();
                    res1001.getDRIVE_REC().getUSER_NO();
                    res1001.getDRIVE_REC().getUSER_NM();
                    res1001.getDRIVE_REC().getDRV_BF_DSTN_KM();
                    res1001.getDRIVE_REC().getDRV_AF_DSTN_KM();
                    res1001.getDRIVE_REC().getDRV_DSTN_KM();
                    res1001.getDRIVE_REC().getUSE_TYPE();
                    res1001.getDRIVE_REC().getDRV_AF_PIC_FILE_IDNT_ID();
                    res1001.getDRIVE_REC().getDRV_AF_PIC_URL();
                    res1001.getDRIVE_REC().getDRV_AF_PIC_FILE_NM();
                    //add into object
                    homeModelRespond = new HomeModelRespond();
                    homeModelRespond.setYYYYMMDD(resRec.getYYYYMMDD());
                    homeModelRespond.setUSE_TYPE(resRec.getUSE_TYPE());
                    homeModelRespond.setDRV_DSTN_KM(resRec.getDRV_DSTN_KM());
                    homeModelRespond.setCAR_TYPE(resRec.getCAR_TYPE());
                    homeModelRespond.setDRV_BF_DSTN_KM(resRec.getDRV_BF_DSTN_KM());
                    homeModelRespond.setDRV_AF_DSTN_KM(resRec.getDRV_AF_DSTN_KM());
                    homeModelRespond.setCAR_NO(resRec.getCAR_NO());
                    biz_name.setText(resRec.getBIZ_NM());
                    user_name.setText(resRec.getUSER_NM());
                    //add into ArrayList
                    homeModelRespondList.add(homeModelRespond);
                    adapter.setdate(homeModelRespondList);
                    //for move to next record
                    resRec.moveNext();
                }
                adapter.notifyDataSetChanged();
                if (resRec.getLength() == 0){
                    recyclerView.setVisibility(View.GONE);
                    no_data.setVisibility(View.VISIBLE);
                }else {
                    recyclerView.setVisibility(View.VISIBLE);
                    no_data.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        },1000);

    }

    //request error
    @Override
     public void onTranError(String tranCode) {
        fragmentLoading.dismiss();
    }
    }