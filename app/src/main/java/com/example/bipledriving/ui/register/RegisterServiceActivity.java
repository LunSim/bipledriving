package com.example.bipledriving.ui.register;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.bipledriving.R;
import com.example.bipledriving.com.FlexibleMenu;
import com.example.bipledriving.model.login.Singleton;
import com.example.bipledriving.ui.home.HomeActivity;
import com.example.bipledriving.ui.fragment.RegisterPopupFragment;
import khmertrainers.com.mylibrary.toolbar.FlexibleToolBar;

public class RegisterServiceActivity extends AppCompatActivity implements View.OnClickListener{
    private FlexibleToolBar mToolBar;
    private RegisterPopupFragment registerPopupFragment;
    private ImageView choseImage;
    private TextView tv_res_vehicle,tv_res_date,tv_res_division,tv_res_far_before,tv_res_far,tv_res_far_drive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_service);

        choseImage          =   findViewById(R.id.choseImage);
        tv_res_vehicle      =   findViewById(R.id.tv_res_vehicle);
        tv_res_date         =   findViewById(R.id.tv_res_date);
        tv_res_division     =   findViewById(R.id.tv_res_division);
        tv_res_far_before   =   findViewById(R.id.tv_res_far_before);
        tv_res_far          =   findViewById(R.id.tv_res_far);
        tv_res_far_drive    =   findViewById(R.id.tv_res_far_drive);

        registerPopupFragment = new RegisterPopupFragment(this);
        mToolBar = findViewById(R.id.toolbar_register_server);
        mToolBar.setToolBarButton(FlexibleToolBar.GB_LEFT,R.drawable.ic_clear,20,0);
        mToolBar.setToolBarButton(FlexibleToolBar.GB_LEFT,getResources().getString(R.string.CREATE_12),20,0);
        mToolBar.setToolBarButton(FlexibleToolBar.GB_RIGHT,R.drawable.ic_menu,0,20);
        mToolBar.setOnToolBarClickListener((groupButton, view) ->  {
            if (groupButton == FlexibleToolBar.GB_RIGHT){
                switch (view.getId()){
                    case 1:
                        showPopUpItem(view);
                        Toast.makeText(this, "cancle", Toast.LENGTH_SHORT).show();
                        break;
                }
            }else if (groupButton == FlexibleToolBar.GB_LEFT){
                switch (view.getId()){
                    case 1:
                        Intent in = new Intent(RegisterServiceActivity.this, HomeActivity.class);
                        startActivity(in);
                        finish();
                        break;
                }
            }
        });
        
        getViewDetail();
        choseImage.setOnClickListener(this);
    }

    //get data for view
    private void getViewDetail() {
        tv_res_vehicle.setText(Singleton.getInstance().getHomeModelRespond().getCAR_NO());
        tv_res_date.setText(getDateformat(Singleton.getInstance().getHomeModelRespond().getYYYYMMDD()));
        tv_res_division.setText(Singleton.getInstance().getHomeModelRespond().getCAR_TYPE());
        tv_res_far_before.setText(Singleton.getInstance().getHomeModelRespond().getDRV_BF_DSTN_KM());
        tv_res_far.setText(Singleton.getInstance().getHomeModelRespond().getDRV_AF_DSTN_KM());
        tv_res_far_drive.setText(Singleton.getInstance().getHomeModelRespond().getDRV_DSTN_KM());
    }

    //menu bar
    public void showPopUpItem(View view){
        String[] items = {"수정하기","삭제하기"};
        FlexibleMenu mMenuPopup = new FlexibleMenu(RegisterServiceActivity.this, 0, 360, 120, 0);
        mMenuPopup.setMenuPadding(30, 0, 0, 0);
        mMenuPopup.setMenuShow(FlexibleMenu.SHOW_AS_DROPDOWN,FlexibleMenu.RIGHT_ANCHOR,view,190,15);
        mMenuPopup.setMenuItem(items, 15, R.color.color_263238, R.color.color_f87499, R.color.color_eff1f5, true);
        mMenuPopup.setOnMenuItemClickListener(new FlexibleMenu.onFlexibleMenuListener() {
            @Override
            public void onMenuItemClicked(int index) {
                switch (index){
                    case 0:
                        registerPopupFragment.show();
                        Toast.makeText(RegisterServiceActivity.this, "cancle1", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        Toast.makeText(RegisterServiceActivity.this, "Do it more!", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }

    //event onClick
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.choseImage:
                DialogChoseImageFragment dialogChoseImageFragment = new DialogChoseImageFragment();
                dialogChoseImageFragment.show(getSupportFragmentManager(),"ShowDialogBottom");
                break;
        }
    }

    //format display data
    private String getDateformat(String date){
        String krDate = "";
        if(!date.equals("")) {
            krDate = date.substring(0,4)+getBaseContext().getResources().getString(R.string.MAIN_10)+date.substring(4,6)+getBaseContext().getResources().getString(R.string.MAIN_04)+date.substring(6,8) + getBaseContext().getResources().getString(R.string.MAIN_05);
        }
        return krDate;
    }
}
