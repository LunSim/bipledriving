package com.example.bipledriving.ui.fragment;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Window;
import com.example.bipledriving.R;

public class RegisterPopupFragment extends Dialog {

    public RegisterPopupFragment(@NonNull Context context) {
        super(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.fragment_register_popup);
    }
}
