package com.example.bipledriving.ui.login;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.example.bipledriving.R;
import com.example.bipledriving.helper.Comtran;
import com.example.bipledriving.model.login.Singleton;
import com.example.bipledriving.model.login.User;
import com.example.bipledriving.tx.api_login.TX_CARMN_0000_REQ;
import com.example.bipledriving.tx.api_login.TX_CARMN_0000_RES;
import com.example.bipledriving.ui.home.HomeActivity;
import com.example.bipledriving.ui.fragment.loading.FragmentLoading;

import khmertrainers.com.mylibrary.HelpPassTransform;
import khmertrainers.com.mylibrary.network.encryp.XorCrypto;
import khmertrainers.com.mylibrary.toolbar.FlexibleToolBar;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, Comtran.OnComTranListener, View.OnTouchListener {
    private ImageView checkBox,hide_password,clear_username,img_loading;
    private Button btnLogin,btn_find_id;
    private EditText mName,mPassword;
    private SharedPreferences.Editor editor;
    private FlexibleToolBar mFlexbleToolBar;
    private User user;
    private String username;
    private String userpass;
    private static boolean isChecked = false;
    private static boolean isClicked = false;
    private FragmentLoading fragmentLoading;
    public HelpPassTransform helpPassTransform;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getCheckRemember();
        Initialization();
        helpPassTransform = new HelpPassTransform(HelpPassTransform.DOT);
    }

    /*Initialization object*/
    private void Initialization() {
        //loading
        fragmentLoading = new FragmentLoading();

        checkBox        =   findViewById(R.id.checkbox_login);
        hide_password   =   findViewById(R.id.hide_pass);
        clear_username  =   findViewById(R.id.clear_username);
        btnLogin        =   findViewById(R.id.btn_login);
        btn_find_id     =   findViewById(R.id.btn_find_id);
        mName           =   findViewById(R.id.ed_username);
        mPassword       =   findViewById(R.id.ed_password);
        //store for remember login
        username = mName.getText().toString();
        userpass = mPassword.getText().toString();

        mFlexbleToolBar = findViewById(R.id.cus_toolbar);
        mFlexbleToolBar.setToolBarTitle(getString(R.string.login_toolbar));

        /*register event*/
        clear_username.setOnTouchListener(this);
        btnLogin.setOnClickListener(this);
        hide_password.setOnTouchListener(this);
        checkBox.setOnClickListener(this);
        mName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (mName.getText().toString().isEmpty()){
                    clear_username.setVisibility(View.GONE);
                }else {
                    clear_username.setVisibility(View.VISIBLE);
                }
            }
        });

        mPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (mPassword.getText().toString().isEmpty()){
                    hide_password.setVisibility(View.GONE);
                }else {
                    hide_password.setVisibility(View.VISIBLE);

                }
            }
        });

    }


    public void getCheckRemember(){
        SharedPreferences pref = getSharedPreferences("bipledriving",MODE_PRIVATE);
        username = pref.getString("user",null);
        userpass = pref.getString("pass",null);
        Log.e(">>>>","RememberChecked::"+username+","+userpass);
        if (username != null && userpass != null){
            RememberLogin();
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_login:
                if (mName.getText().toString().equals("sbook02") && mPassword.getText().toString().equals("1q2w3e4r!")){
                    new Handler().post(() -> {
                        fragmentLoading = new FragmentLoading();
                        fragmentLoading.show(getSupportFragmentManager(),"FragmentLoading");
                        loginFirst();
                    });
                }else {
                    LayoutInflater inflater = getLayoutInflater();
                    @SuppressLint("WrongViewCast") View toast = inflater.inflate(R.layout.custom_toast,(ViewGroup)findViewById(R.id.customToast));
                    Toast t = new Toast(getApplicationContext());
                    t.setDuration(Toast.LENGTH_LONG);
                    t.setView(toast);
                    t.show();
                }
                break;
            case R.id.checkbox_login:
                    if (isChecked){
                        isChecked = false;
                        checkBox.setImageResource(R.drawable.chkbox_off_icon);
                    }else {
                        isChecked = true;
                        checkBox.setImageResource(R.drawable.chkbox_on_icon);
                        SharedPreferences sharedPreferences = getSharedPreferences("bipledriving", MODE_PRIVATE);
                        editor = sharedPreferences.edit();
                        editor.putString("user", mName.getText().toString());
                        editor.putString("pass",mPassword.getText().toString()).apply();
                        Log.e(">>>","When::Clicked::Remember::"+mName.getText().toString()+mPassword.getText().toString());
                    }
                break;
        }
    }

    /*API Respond*/
    @Override
    public void onTranRespond(String tranCode, Object object) {
        try {
            TX_CARMN_0000_RES tx_carmn_0000_res = new TX_CARMN_0000_RES(object);
            tx_carmn_0000_res.getUSER_ID();
            user = new User();
            user.setId(mName.getText().toString());
            user.setName(mPassword.getText().toString());
            Singleton.getInstance().setUser(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onTranError(String tranCode) {}

    public void loginFirst(){
            Comtran comtran = new Comtran(this,this);
            try {
                TX_CARMN_0000_REQ req0000 = new TX_CARMN_0000_REQ();
                req0000.setMOBL_CD("001");
                req0000.setUSER_ID(mName.getText().toString());
                req0000.setUSER_PW(XorCrypto.encoding(mPassword.getText().toString()));
                comtran.request(TX_CARMN_0000_REQ.TXNO, req0000.getSendMessage(), false);
                Log.e(">>>>", "Object1:::" + req0000.getSendMessage());
            } catch (Exception e) {
                e.getStackTrace();
            }
    }

    public void RememberLogin(){
        Comtran comtran = new Comtran(this, this);
            try {
                TX_CARMN_0000_REQ req0000 = new TX_CARMN_0000_REQ();
                req0000.setMOBL_CD("001");
                req0000.setUSER_ID(username);
                req0000.setUSER_PW(XorCrypto.encoding(userpass));
                comtran.request(TX_CARMN_0000_REQ.TXNO, req0000.getSendMessage(), false);
                Log.e(">>>>", "Object1:::" + req0000.getSendMessage());
            } catch (Exception e) {
                e.getStackTrace();
            }
    }

    //when click delete editText username
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (view.getId() == R.id.clear_username){
            mName.setText("");
        }else if (view.getId() == R.id.hide_pass){
            if (isClicked){
                isClicked = false;
                hide_password.setImageResource(R.drawable.pwd_eyes_off);
                mPassword.setTransformationMethod(helpPassTransform);
            }else {
                isClicked = true;
                hide_password.setImageResource(R.drawable.pwd_eyes_on);
                mPassword.setTransformationMethod(null);
            }
        }

        return false;
    }
}