package com.example.bipledriving.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.bipledriving.R;

public class FragmentFilter  extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_filter,container,false);
        ImageView imageView = view.findViewById(R.id.close_filter);
        imageView.setOnClickListener(view1 -> {
            for (Fragment fragment : getActivity().getSupportFragmentManager().getFragments()) {
                if (fragment instanceof FragmentFilter) {
                    getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
                }
            }
        });
        return view;
    }
}
