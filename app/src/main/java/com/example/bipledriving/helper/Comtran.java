package com.example.bipledriving.helper;
import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;

import khmertrainers.com.mylibrary.network.VolleyNetwork;
import khmertrainers.com.mylibrary.network.internal.OnNetworkListener;
import khmertrainers.com.mylibrary.network.tx.JSONHelper;

public class Comtran implements OnNetworkListener {

    /**
     *  Uset Agent 초기 값
     */
    public static String mUserAgent = "Mozilla/5.0 (Linux; U; Android 2.1; en-us; sdk Build/ERD79) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17";
    private Context context;
    private OnComTranListener mListener;
    private VolleyNetwork mVolley;

    public Comtran(Context context, OnComTranListener mListener){
        this.context = context;
        this.mListener = mListener;
        mVolley = new VolleyNetwork(context, this);
    }

    //request api
    public void request(final String aipId, HashMap<String, Object> req_data, boolean dialog) throws  Exception{
        JSONObject jsonObject = (JSONObject) JSONHelper.toJSON(req_data);
        JSONObject jsonInput = new JSONObject();

        jsonInput.put(ComtranCode.KEY_SVC_GB, "001");
        jsonInput.put(ComtranCode.CNTS_CRTS_KEY, "5766689d-3c57-4246-a6a2-cfb451681d2b");
        jsonInput.put(ComtranCode.KEY_API_ID, aipId);
        jsonInput.put(ComtranCode.KEY_REQ_DATA, jsonObject);

        //Helper
        HashMap<String, String>headers = new HashMap<>();
        headers.put("charset", "UTF-8");
        headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
        headers.put("User-Agent",mUserAgent);
        mVolley.setComHeaders(headers);

        try {
            HashMap<String, String> params = new HashMap<>();
            params.put("JSONData", URLEncoder.encode(jsonInput.toString(), "UTF-8"));
            mVolley.requestVolleyNetwork(aipId, true,"http://dev.webank.appplay.co.kr/CarmnGate.do", params, false);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onNetworkResponse(String tranCode, Object object) {
        //use when network request respond
        Log.e("data","Show:::"+object);
        JSONObject jsonOutput;
        JSONArray jsonArray = null;

        try {
            jsonOutput = new JSONObject(URLDecoder.decode(object.toString(), "UTF-8"));

            if (!jsonOutput.isNull(ComtranCode.KEY_API_ID)){
                String resultError = jsonOutput.getString(ComtranCode.KEY_RESL_CD);
                mListener.onTranError( resultError);
                return;
            }
            if (!jsonOutput.isNull(ComtranCode.KEY_RES_DATA)){
                jsonArray = new JSONArray();
                jsonArray.put(jsonOutput.getJSONArray(ComtranCode.KEY_RES_DATA).get(0));
            }
            Log.e("data","Show:::"+jsonOutput.toString(1));

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        mListener.onTranRespond(tranCode, jsonArray);
    }

    @Override
    public void onNetworkError(String tranCode, Object object) {
        //Network not respond
    }

    public interface OnComTranListener {
        void onTranRespond(String tranCode, Object object);
        void onTranError(String tranCode);
    }

    private class  ComtranCode{
        static final  String KEY_REQ_DATA = "REQ_DATA";
        static final  String KEY_API_ID = "API_ID";
        static final  String KEY_SVC_GB = "SVC_GB";
        static final  String CNTS_CRTS_KEY = "CNTS_CRTS_KEY";
        static final  String KEY_RESL_CD = "RESL_CD";
        static final  String CNTS_CRTS_MSG = "RESL_MSG";
        static final  String KEY_RES_DATA = "RESP_DATA";
    }
}
