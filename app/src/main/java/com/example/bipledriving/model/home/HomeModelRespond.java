package com.example.bipledriving.model.home;

public class HomeModelRespond {
    private String SQNO;
    private String BIZ_NO;
    private String BIZ_NM;
    private String CAR_NO;
    private String CAR_TYPE;
    private String MM;
    private String YYYYMMDD;
    private String DVSN_NM;
    private String USER_NO;
    private String USER_NM;
    private String DRV_BF_DSTN_KM;
    private String DRV_AF_DSTN_KM;
    private String DRV_DSTN_KM;
    private String USE_TYPE;
    private String DRV_AF_PIC_FILE_IDNT_ID;
    private String DRV_AF_PIC_URL;
    private String DRV_AF_PIC_FILE_NM;

    public HomeModelRespond() {
    }

    public HomeModelRespond(String SQNO, String BIZ_NO, String BIZ_NM, String CAR_NO, String CAR_TYPE, String MM, String YYYYMMDD, String DVSN_NM, String USER_NO, String USER_NM, String DRV_BF_DSTN_KM, String DRV_AF_DSTN_KM, String DRV_DSTN_KM, String USE_TYPE, String DRV_AF_PIC_FILE_IDNT_ID, String DRV_AF_PIC_URL, String DRV_AF_PIC_FILE_NM) {
        this.SQNO   = SQNO;
        this.BIZ_NO = BIZ_NO;
        this.BIZ_NM = BIZ_NM;
        this.CAR_NO = CAR_NO;
        this.CAR_TYPE = CAR_TYPE;
        this.MM = MM;
        this.YYYYMMDD = YYYYMMDD;
        this.DVSN_NM = DVSN_NM;
        this.USER_NO = USER_NO;
        this.USER_NM = USER_NM;
        this.DRV_BF_DSTN_KM = DRV_BF_DSTN_KM;
        this.DRV_AF_DSTN_KM = DRV_AF_DSTN_KM;
        this.DRV_DSTN_KM = DRV_DSTN_KM;
        this.USE_TYPE = USE_TYPE;
        this.DRV_AF_PIC_FILE_IDNT_ID = DRV_AF_PIC_FILE_IDNT_ID;
        this.DRV_AF_PIC_URL = DRV_AF_PIC_URL;
        this.DRV_AF_PIC_FILE_NM = DRV_AF_PIC_FILE_NM;
    }

    public String getSQNO() {
        return SQNO;
    }

    public void setSQNO(String SQNO) {
        this.SQNO = SQNO;
    }

    public String getBIZ_NO() {
        return BIZ_NO;
    }

    public void setBIZ_NO(String BIZ_NO) {
        this.BIZ_NO = BIZ_NO;
    }

    public String getBIZ_NM() {
        return BIZ_NM;
    }

    public void setBIZ_NM(String BIZ_NM) {
        this.BIZ_NM = BIZ_NM;
    }

    public String getCAR_NO() {
        return CAR_NO;
    }

    public void setCAR_NO(String CAR_NO) {
        this.CAR_NO = CAR_NO;
    }

    public String getCAR_TYPE() {
        return CAR_TYPE;
    }

    public void setCAR_TYPE(String CAR_TYPE) {
        this.CAR_TYPE = CAR_TYPE;
    }

    public String getMM() {
        return MM;
    }

    public void setMM(String MM) {
        this.MM = MM;
    }

    public String getYYYYMMDD() {
        return YYYYMMDD;
    }

    public void setYYYYMMDD(String YYYYMMDD) {
        this.YYYYMMDD = YYYYMMDD;
    }

    public String getDVSN_NM() {
        return DVSN_NM;
    }

    public void setDVSN_NM(String DVSN_NM) {
        this.DVSN_NM = DVSN_NM;
    }

    public String getUSER_NO() {
        return USER_NO;
    }

    public void setUSER_NO(String USER_NO) {
        this.USER_NO = USER_NO;
    }

    public String getUSER_NM() {
        return USER_NM;
    }

    public void setUSER_NM(String USER_NM) {
        this.USER_NM = USER_NM;
    }

    public String getDRV_BF_DSTN_KM() {
        return DRV_BF_DSTN_KM;
    }

    public void setDRV_BF_DSTN_KM(String DRV_BF_DSTN_KM) {
        this.DRV_BF_DSTN_KM = DRV_BF_DSTN_KM;
    }

    public String getDRV_AF_DSTN_KM() {
        return DRV_AF_DSTN_KM;
    }

    public void setDRV_AF_DSTN_KM(String DRV_AF_DSTN_KM) {
        this.DRV_AF_DSTN_KM = DRV_AF_DSTN_KM;
    }

    public String getDRV_DSTN_KM() {
        return DRV_DSTN_KM;
    }

    public void setDRV_DSTN_KM(String DRV_DSTN_KM) {
        this.DRV_DSTN_KM = DRV_DSTN_KM;
    }

    public String getUSE_TYPE() {
        return USE_TYPE;
    }

    public String setUSE_TYPE(String USE_TYPE) {
        this.USE_TYPE = USE_TYPE;
        return USE_TYPE;
    }

    public String getDRV_AF_PIC_FILE_IDNT_ID() {
        return DRV_AF_PIC_FILE_IDNT_ID;
    }

    public void setDRV_AF_PIC_FILE_IDNT_ID(String DRV_AF_PIC_FILE_IDNT_ID) {
        this.DRV_AF_PIC_FILE_IDNT_ID = DRV_AF_PIC_FILE_IDNT_ID;
    }

    public String getDRV_AF_PIC_URL() {
        return DRV_AF_PIC_URL;
    }

    public void setDRV_AF_PIC_URL(String DRV_AF_PIC_URL) {
        this.DRV_AF_PIC_URL = DRV_AF_PIC_URL;
    }

    public String getDRV_AF_PIC_FILE_NM() {
        return DRV_AF_PIC_FILE_NM;
    }

    public void setDRV_AF_PIC_FILE_NM(String DRV_AF_PIC_FILE_NM) {
        this.DRV_AF_PIC_FILE_NM = DRV_AF_PIC_FILE_NM;
    }
}
