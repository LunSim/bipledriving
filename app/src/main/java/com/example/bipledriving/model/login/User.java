package com.example.bipledriving.model.login;

import java.util.ArrayList;

public class User {
    private String id;
    private String name;
    private String image_path;
    private ArrayList<BIZ_REC> biz_recArrayList;

    public User() {
    }

    public User(String id, String name, String image_path, ArrayList<BIZ_REC> biz_recArrayList) {
        this.id = id;
        this.name = name;
        this.image_path = image_path;
        this.biz_recArrayList = biz_recArrayList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public ArrayList<BIZ_REC> getBiz_recArrayList() {
        return biz_recArrayList;
    }

    public void setBiz_recArrayList(ArrayList<BIZ_REC> biz_recArrayList) {
        this.biz_recArrayList = biz_recArrayList;
    }

    public  class BIZ_REC{
        private String biz_no;
        private String biz_nm;

        public BIZ_REC() {
        }

        public BIZ_REC(String biz_no, String biz_nm) {
            this.biz_no = biz_no;
            this.biz_nm = biz_nm;
        }

        public String getBiz_no() {
            return biz_no;
        }

        public void setBiz_no(String biz_no) {
            this.biz_no = biz_no;
        }

        public String getBiz_nm() {
            return biz_nm;
        }

        public void setBiz_nm(String biz_nm) {
            this.biz_nm = biz_nm;
        }
    }
}
