package com.example.bipledriving.model.login;

import com.example.bipledriving.model.home.HomeModelRespond;

public class Singleton {
    private User user;
    private HomeModelRespond homeModelRespond;
    private static Singleton instance;

    public Singleton() {
    }

    public static Singleton getInstance() {
        if (instance == null)
            instance = new Singleton();
        return instance;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public HomeModelRespond getHomeModelRespond() {
        return homeModelRespond;
    }

    public void setHomeModelRespond(HomeModelRespond homeModelRespond) {
        this.homeModelRespond = homeModelRespond;
    }

    public static void setInstance(Singleton instance) {
        Singleton.instance = instance;
    }
}
