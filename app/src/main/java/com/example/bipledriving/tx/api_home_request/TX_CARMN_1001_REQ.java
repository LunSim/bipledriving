package com.example.bipledriving.tx.api_home_request;
import khmertrainers.com.mylibrary.network.tx.TxMessage;

public class TX_CARMN_1001_REQ extends TxMessage {
    //key api
    public static final String  TXNO = "CARMN_1001";
    private TX_CARMN_1001_REQ_DATA mTxKeyData;

    //request api
    public TX_CARMN_1001_REQ()throws Exception {
        mTxKeyData = new TX_CARMN_1001_REQ.TX_CARMN_1001_REQ_DATA();
        super.initSendMessage();
    }

    public void setBIZ_NO(String value){
        mSendMessage.put(mTxKeyData.BIZ_NO, value);
    }

    public void setUSER_ID(String value){
        mSendMessage.put(mTxKeyData.USER_ID, value);
    }

    public void setSTR_DT(String value){
        mSendMessage.put(mTxKeyData.STR_DT, value);
    }
    public void setEND_DT(String value){
        mSendMessage.put(mTxKeyData.END_DT, value);
    }
    public void setUSE_TYPE(String value){
        mSendMessage.put(mTxKeyData.USE_TYPE, value);
    }
    public void setCAR_NO(String value){
        mSendMessage.put(mTxKeyData.CAR_NO, value);
    }
    public void setYYYYMMDD(String value){
        mSendMessage.put(mTxKeyData.YYYYMMDD, value);
    }
    public void setSQNO(String value){
        mSendMessage.put(mTxKeyData.SQNO, value);
    }
    public void setSRCH_WD(String value){
        mSendMessage.put(mTxKeyData.SRCH_WD, value);
    }
    private class TX_CARMN_1001_REQ_DATA{
        private String BIZ_NO   =   "BIZ_NO";
        private String USER_ID	=   "USER_ID";
        private String STR_DT   =   "STR_DT";
        private String END_DT   =   "END_DT";
        private String USE_TYPE =   "USE_TYPE";
        private String CAR_NO   =   "CAR_NO";
        private String YYYYMMDD =   "YYYYMMDD";
        private String SQNO     =   "SQNO";
        private String SRCH_WD  =   "SRCH_WD";
    }
}
