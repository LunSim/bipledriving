package com.example.bipledriving.tx.api_login;

import khmertrainers.com.mylibrary.network.tx.TxMessage;

public class TX_CARMN_0000_RES extends TxMessage {

    private  TX_CARMN_0000_RES_DATA mTxKeyData;

    public  TX_CARMN_0000_RES(Object object) throws Exception {
        mTxKeyData = new TX_CARMN_0000_RES_DATA();
        super.initRecvMessage(object);
    }
    //get record
    public TX_CARMN_0000_RES_REC getBIZ_REC()throws Exception{
        return new TX_CARMN_0000_RES_REC(getRecord(mTxKeyData.BIZ_REC));
    }

    public String getUSER_ID() throws Exception{
        return getString(mTxKeyData.USER_ID);
    }
    public String getUSER_NM() throws Exception{
        return getString(mTxKeyData.USER_NM);
    }
    public String getUSER_IMG_PATH() throws Exception{
        return getString(mTxKeyData.USER_IMG_PATH);
    }
    public String getBSNN_NM() throws Exception{
        return getString(mTxKeyData.BSNN_NM);
    }
    public String getCRTC_PATH() throws Exception{
        return getString(mTxKeyData.CRTC_PATH);
    }
    public String getPTL_ID() throws Exception{
        return getString(mTxKeyData.PTL_ID);
    }
    public String getUSE_INTT_ID() throws Exception{
        return getString(mTxKeyData.USE_INTT_ID);
    }
    public String getCHNL_ID() throws Exception{
        return getString(mTxKeyData.CHNL_ID);
    }
   private class TX_CARMN_0000_RES_DATA{
       private String USER_ID       =   "USER_ID";
       private String USER_NM       =   "USER_NM";
       private String USER_IMG_PATH =   "USER_IMG_PATH";
       private String BSNN_NM       =   "BSNN_NM";
       private String CRTC_PATH     =   "CRTC_PATH";
       private String BIZ_REC       =   "BIZ_REC";
       private String PTL_ID        =   "PTL_ID";
       private String USE_INTT_ID   =   "USE_INTT_ID";
       private String CHNL_ID       =   "CHNL_ID";
   }
}
