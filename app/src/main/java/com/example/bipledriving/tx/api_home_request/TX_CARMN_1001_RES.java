package com.example.bipledriving.tx.api_home_request;

import khmertrainers.com.mylibrary.network.tx.TxMessage;

public class TX_CARMN_1001_RES extends TxMessage {
    private TX_CARMN_1001_RES_DATA mTxKeyData;

    public TX_CARMN_1001_RES(Object object)throws Exception{
        mTxKeyData = new TX_CARMN_1001_RES_DATA();
        super.initRecvMessage(object);
    }
    //
    public TX_CARMN_1001_RES_REC getDRIVE_REC() throws Exception{
        return new TX_CARMN_1001_RES_REC(getRecord(mTxKeyData.DRIVE_REC));
    }

    private class TX_CARMN_1001_RES_DATA{
        private String DRIVE_REC = "DRIVE_REC";
    }
}
