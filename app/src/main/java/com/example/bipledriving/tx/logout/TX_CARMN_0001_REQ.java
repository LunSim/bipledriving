package com.example.bipledriving.tx.logout;

import khmertrainers.com.mylibrary.network.tx.TxMessage;

public class TX_CARMN_0001_REQ extends TxMessage {
    public static final String  TXNO = "CARMN_0001";
    private  TX_CARMN_0001_REQ_DATA mTxKeyData;

    public TX_CARMN_0001_REQ(Object object)throws Exception {
        mTxKeyData = new TX_CARMN_0001_REQ_DATA();
        super.initSendMessage();
    }

    public TX_CARMN_0001_REQ() {

    }

    public void setUSER_ID(String value){
        mSendMessage.put(mTxKeyData.USERID, value);
    }

    private class TX_CARMN_0001_REQ_DATA{
        private String USERID  = "USER_ID";
    }
}
