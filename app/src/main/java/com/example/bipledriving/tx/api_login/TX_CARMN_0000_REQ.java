package com.example.bipledriving.tx.api_login;

import khmertrainers.com.mylibrary.network.tx.TxMessage;

public class TX_CARMN_0000_REQ extends TxMessage {
    //key for request API
    public static final String  TXNO = "CARMN_0000";
    //create obj
    private  TX_CARMN_0000_REQ_DATA mTxKeyData;
    /*request api*/
    public TX_CARMN_0000_REQ()throws Exception {
        mTxKeyData = new TX_CARMN_0000_REQ_DATA();
        super.initSendMessage();
    }

    public void setUSER_ID(String value){
        mSendMessage.put(mTxKeyData.USERID, value);
    }

    public void setUSER_PW(String value){
        mSendMessage.put(mTxKeyData.USER_PW, value);
    }

    public void setMOBL_CD(String value){
        mSendMessage.put(mTxKeyData.MOBL_CD, value);
    }

    private class TX_CARMN_0000_REQ_DATA{
        private String USERID  = "USER_ID";
        private String USER_PW = "USER_PW";
        private String MOBL_CD = "MOBL_CD";
    }

}
