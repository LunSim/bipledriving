package com.example.bipledriving.tx.api_home_request;

import khmertrainers.com.mylibrary.network.tx.TxMessage;

public class TX_CARMN_1001_RES_REC extends TxMessage {
    private TX_CARMN_1001_RES_DATA mTxKeyData;

    public TX_CARMN_1001_RES_REC(Object object) throws Exception{
        mTxKeyData = new TX_CARMN_1001_RES_DATA();
        super.initRecvMessage(object);
    }

    public String getSQNO()throws Exception{
        return getString(mTxKeyData.SQNO);
    }
    public String getBIZ_NO()throws Exception{
        return getString(mTxKeyData.BIZ_NO);
    }
    public String getBIZ_NM()throws Exception{
        return getString(mTxKeyData.BIZ_NM);
    }
    public String getCAR_NO()throws Exception{
        return getString(mTxKeyData.CAR_NO);
    }
    public String getCAR_TYPE()throws Exception{
        return getString(mTxKeyData.CAR_TYPE);
    }
    public String getMM()throws Exception{
        return getString(mTxKeyData.MM);
    }
    public String getYYYYMMDD()throws Exception{
        return getString(mTxKeyData.YYYYMMDD);
    }
    public String getDVSN_NM()throws Exception{
        return getString(mTxKeyData.DVSN_NM);
    }
    public String getUSER_NO()throws Exception{
        return getString(mTxKeyData.USER_NO);
    }
    public String getUSER_NM()throws Exception{
        return getString(mTxKeyData.USER_NM);
    }
    public String getDRV_BF_DSTN_KM()throws Exception{
        return getString(mTxKeyData.DRV_BF_DSTN_KM);
    }
    public String getDRV_AF_DSTN_KM()throws Exception{
        return getString(mTxKeyData.DRV_AF_DSTN_KM);
    }
    public String getDRV_DSTN_KM()throws Exception{
        return getString(mTxKeyData.DRV_DSTN_KM);
    }
    public String getUSE_TYPE()throws Exception{
        return getString(mTxKeyData.USE_TYPE);
    }
    public String getDRV_AF_PIC_FILE_IDNT_ID()throws Exception{
        return getString(mTxKeyData.DRV_AF_PIC_FILE_IDNT_ID);
    }
    public String getDRV_AF_PIC_URL()throws Exception{
        return getString(mTxKeyData.DRV_AF_PIC_URL);
    }
    public String getDRV_AF_PIC_FILE_NM()throws Exception{
        return getString(mTxKeyData.DRV_AF_PIC_FILE_NM);
    }

    private class TX_CARMN_1001_RES_DATA{
        private String SQNO                     =   "SQNO";
        private String BIZ_NO                   =   "BIZ_NO";
        private String BIZ_NM                   =   "BIZ_NM";
        private String CAR_NO                   =   "CAR_NO";
        private String CAR_TYPE                 =   "CAR_TYPE";
        private String MM                       =   "MM";
        private String YYYYMMDD                 =   "YYYYMMDD";
        private String DVSN_NM                  =   "DVSN_NM";
        private String USER_NO                  =   "USER_NO";
        private String USER_NM                  =   "USER_NM";
        private String DRV_BF_DSTN_KM           =   "DRV_BF_DSTN_KM";
        private String DRV_AF_DSTN_KM           =   "DRV_AF_DSTN_KM";
        private String DRV_DSTN_KM              =   "DRV_DSTN_KM";
        private String USE_TYPE                 =   "USE_TYPE";
        private String DRV_AF_PIC_FILE_IDNT_ID  =   "DRV_AF_PIC_FILE_IDNT_ID";
        private String DRV_AF_PIC_URL           =   "DRV_AF_PIC_URL";
        private String DRV_AF_PIC_FILE_NM       =   "DRV_AF_PIC_FILE_NM";
    }
}
