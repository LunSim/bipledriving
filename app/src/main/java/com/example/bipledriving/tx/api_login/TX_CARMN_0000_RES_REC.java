package com.example.bipledriving.tx.api_login;

import khmertrainers.com.mylibrary.network.tx.TxMessage;

public class TX_CARMN_0000_RES_REC extends TxMessage {
    private TX_CARMN_0000_RES_DATA mTxKeyData;

    public TX_CARMN_0000_RES_REC(Object object) throws Exception{
        mTxKeyData = new TX_CARMN_0000_RES_DATA();
        super.initRecvMessage(object);
    }
    public String getBIZ_NO()throws Exception{
        return getString(mTxKeyData.BIZ_NO);
    }
    public String getBIZ_NM()throws Exception{
        return getString(mTxKeyData.BIZ_NM);
    }
    private class TX_CARMN_0000_RES_DATA{
        private String BIZ_NO = "BIZ_NO";
        private String BIZ_NM = "BIZ_NM";

    }
}
