package khmertrainers.com.mylibrary;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class PreferenceDelegator {
    private static PreferenceDelegator mInstance;
    private String mRaw = "d40b1465e1838237e5b1abeeac7bec9a";
    private SharedPreferences mPreferences;

    private PreferenceDelegator(Context context) {
        this.mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static PreferenceDelegator getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new PreferenceDelegator(context);
        }
        return mInstance;
    }

    public boolean contains(String key) {
        return this.mPreferences.contains(key);
    }

    public String get(String key) {
        String returnValue = "";
        try {
            if (this.mPreferences.contains(key)) {
                returnValue = this.mPreferences.getString(key, null);

                SecretKeySpec sKeySpec = new SecretKeySpec(this.mRaw.getBytes(), "AES");

                Cipher cipher = Cipher.getInstance("AES");
                cipher.init(2, sKeySpec);
                returnValue = new String(cipher.doFinal(asByte(returnValue)));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return returnValue;
    }

    public void put(String key, String value) {
        try {
            SecretKeySpec sKeySpec = new SecretKeySpec(this.mRaw.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(1, sKeySpec);
            value = asHex(cipher.doFinal(value.getBytes()));

            this.mPreferences.edit().putString(key, value).commit();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
    }

    private byte[] asByte(String s) {
        int len = s.length();
        byte[] data = new byte[len % 2 == 0 ? len / 2 : len / 2 + 1];

        for (int i = 0; i < len; i += 2) {
            if (i + 1 < len)
                data[(i / 2)] = ((byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16)));
            else {
                data[(i / 2)] = ((byte) (Character.digit(s.charAt(i), 16) << 4));
            }
        }
        return data;
    }

    private String asHex(byte[] buf) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);

        for (int i = 0; i < buf.length; i++) {
            if ((buf[i] & 0xFF) < 16) {
                strbuf.append("0");
            }
            strbuf.append(Long.toString(buf[i] & 0xFF, 16));
        }
        return strbuf.toString();
    }
}

